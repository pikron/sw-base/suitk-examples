#include "tgtspec.h"

suiuser_define_table_t special_action_defines[] = {
  /* NULL */
//  SU_DEF_ONE_ITEM(ACT_GET_KEYTABLE),

  SU_DEF_ONE_ITEM(SUI_RET_OK),
  SU_DEF_ONE_ITEM(SUI_RET_ERR),
  SU_DEF_ONE_ITEM(SUI_RET_NRDY),
  SU_DEF_ONE_ITEM(SUI_RET_NCON),
  SU_DEF_ONE_ITEM(SUI_RET_EPERM),
  SU_DEF_ONE_ITEM(SUI_RET_ETYPE),
  SU_DEF_ONE_ITEM(SUI_RET_EINDX),
  SU_DEF_ONE_ITEM(SUI_RET_EOORP),
  SU_DEF_ONE_ITEM(SUI_RET_EOORN),
  SU_DEF_ONE_ITEM(SUI_RET_ZERODIV),

  {NULL, {0,{0}}}
};

/* ************************************************************************** */
/* demo testing variables */
extern unsigned long get_current_time(void);
//unsigned long currenttime = 0;
int demo_tbldi_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
    if (indx<0 || indx>=dinfo->idxsize) return SUI_RET_ERR;
    *(long *)buf = (unsigned short) get_current_time() / (indx+1);
    return SUI_RET_OK;
}
sui_dinfo_static_with_idxsize(demo_table_dinfo, SUI_TYPE_ULONG, NULL, demo_tbldi_rdval, NULL, 0, 0, 0, 0, 30);

/* demo specific functions */
int act_set_flag(struct sui_application *appl, struct sui_action *action)
{
  if (action && action->wdg_dst) {
    sui_set_flag(action->wdg_dst, action->flags);
    return 0;
  }
  return -1;
}

int act_clear_flag(struct sui_application *appl, struct sui_action *action)
{
  if (action && action->wdg_dst) {
    sui_clear_flag(action->wdg_dst, action->flags);
    return 0;
  }
  return -1;
}

int act_set_align_flag(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst) {
        action->wdg_dst->labalign |= action->flags;
        return 0;
    }
    return -1;
}
int act_clear_align_flag(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst) {
        action->wdg_dst->labalign &= ~action->flags;
        return 0;
    }
    return -1;
}

int act_set_style_align_flag(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst && action->wdg_dst->style) {
        action->wdg_dst->style->labalign |= action->flags;
        return 0;
    }
    return -1;
}
int act_clear_style_align_flag(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst && action->wdg_dst->style) {
        action->wdg_dst->style->labalign &= ~action->flags;
        return 0;
    }
    return -1;
}

int act_set_style_flag(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst && action->wdg_dst->style) {
        action->wdg_dst->style->flags |= action->flags;
        return 0;
    }
    return -1;
}
int act_clear_style_flag(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst && action->wdg_dst->style) {
        action->wdg_dst->style->flags &= ~action->flags;
        return 0;
    }
    return -1;
}

int act_set_style_frame_or_gap(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst && action->wdg_dst->style && action->data_src) {
        long val;
        if (sui_rd_long(action->data_src, 0,&val)!=SUI_RET_OK) return 1;
        if (action->value==0) // frame
            action->wdg_dst->style->frame = val;
        else // gap
            action->wdg_dst->style->gap = val;
        return 0;
    }
    return -1;
}
int act_set_style_color(struct sui_application *appl, struct sui_action *action)
{
    if(action && action->wdg_dst && action->wdg_dst->style && action->wdg_dst->style->coltab && action->data_src) {
        long val;
        if (sui_rd_long(action->data_src, 0,&val)!=SUI_RET_OK) return 1;
        if (action->value>=action->wdg_dst->style->coltab->count) return 2;
        action->wdg_dst->style->coltab->ctab[action->value] = val;
        return 0;
    }
    return -1;
}

int register_all_state_variables(void)
{
  ns_add_object_to_ns( &ns_global_namespace, ns_create_object( "demo_tablevar_di", &demo_table_dinfo, SUI_TYPE_DINFO, 0));

/* action functions */
//  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_flag", act_set_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns( ns_current_namespace, ns_create_object( "sui_hkey_trans_cmd", sui_hkey_trans_cmd, SUI_TYPE_KEYFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns( ns_current_namespace, ns_create_object( "sui_hkey_trans_key", sui_hkey_trans_key, SUI_TYPE_KEYFCN, NSOF_STATIC_ALL));

  suiuser_register_defines_from_table(special_action_defines);

/* creation and registration of sui_last_action_return_code */
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_flag", act_set_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_clear_flag", act_clear_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_align_flag", act_set_align_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_clear_align_flag", act_clear_align_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_style_align_flag", act_set_style_align_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_clear_style_align_flag", act_clear_style_align_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_style_flag", act_set_style_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_clear_style_flag", act_clear_style_flag, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_style_frame_or_gap", act_set_style_frame_or_gap, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));
  ns_add_object_to_ns(&ns_global_namespace, ns_create_object("act_set_style_color", act_set_style_color, SUI_TYPE_ACTIONFCN, NSOF_STATIC_ALL));

  return 0;
}

/* ************************************************************************** */
int sgm_lcd_out_lock(PSD psd)
{
    return 0;
}

int sgm_lcd_out_unlock(PSD psd)
{
    return 0;
}
