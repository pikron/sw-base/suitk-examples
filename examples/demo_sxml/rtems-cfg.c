#include <bsp.h>              /* includes <rtems.h> */
#include <rtems.h>
#include <rtems/shell.h>
#include <pthread.h>
#include <rtems/untar.h>

#if defined(__i386__)
  #include <bsp/tty_drv.h>
  #include <rtems/ps2_drv.h>
  #include <rtems/serial_mouse.h>
#endif

extern int _binary_sxml_tarfile_start;
extern int _binary_sxml_tarfile_size;
#define TARFILE_START _binary_sxml_tarfile_start
#define TARFILE_SIZE _binary_sxml_tarfile_size

#define CONFIGURE_UNIFIED_WORK_AREAS
#define CONFIGURE_UNLIMITED_OBJECTS

#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM
#define CONFIGURE_INIT_TASK_ATTRIBUTES    RTEMS_FLOATING_POINT

#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS      20

#define CONFIGURE_POSIX_INIT_THREAD_STACK_SIZE        (128*1024)
#define CONFIGURE_POSIX_INIT_THREAD_TABLE

#define CONFIGURE_MICROSECONDS_PER_TICK               1000

#define PS2_MOUSE 1
#if PS2_MOUSE
  #define MOUSE_DRIVER PAUX_DRIVER_TABLE_ENTRY
#else
  /*
   * Make sure that you have selected the COM port and the
   * mouse type in ( c/src/lib/libbsp/i386/pc386/console/serial_mouse.h ).
   */
  #define MOUSE_DRIVER SERIAL_MOUSE_DRIVER_TABLE_ENTRY
#endif

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
/*
 *  All BSPs which have frame buffer support define BSP_HAS_FRAME_BUFFER.
 *  So if we don't have support for it statically, then it is the responsibility
 *  of the application to dynamically detect and install one.  Otherwise,
 *  Nano-X should fail very early.
 */
#if (BSP_HAS_FRAME_BUFFER == 1)
  #define CONFIGURE_APPLICATION_NEEDS_FRAME_BUFFER_DRIVER
#endif

#define CONFIGURE_APPLICATION_EXTRA_DRIVERS \
        MOUSE_DRIVER
//      TTY2_DRIVER_TABLE_ENTRY,

extern int ui_main(int argc, char *argv[]);

static char *arg_array[] = {
  "demo_sxml",
  "start.sxml"
};

void *POSIX_Init( void *argument )
{
  rtems_status_code status;

  status = Untar_FromMemory((unsigned char *)(&TARFILE_START), (long)&TARFILE_SIZE);

  printf("Untar_FromMemory returned %s\n",rtems_status_text(status));

  //setenv( "HOME", "/", 1 );
  //setenv( "T1LIB_CONFIG", "/fonts/t1lib/t1lib.config", 1 );

  ui_main(2, arg_array);

  pthread_exit( NULL );
  return NULL; /* just so the compiler thinks we returned something */
}

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
