/**
 * SuiTk Demo
 */

#include <stdio.h>
#include <time.h>

#include <suitk/suitk.h>
#include <suixml/sui_sxml.h>
#include <suixml/sxml_suitk.h>
#include <suixml/sui_defines.h>

#include <ul_evpoll.h>

#include <ul_log.h>
UL_LOG_CUST(ulogd_mainappl)
#define UL_LOGL_DEF UL_LOGL_INF
#include "log_domains.inc"

#include "tgtspec.h"

/******************************************************************************/
/* Application operating mode */
int appl_op_mode;
int appl_error_code;
int appl_use_simple_main_loop;

/* to prevent pull into link libmwin.a(winmain.o):WinMain function */
int escape_quits;

/******************************************************************************/
/* table of states and their scenarios */
/* FIXME: move to better place  and need revise */
const utf8 *get_scenario_from_mode( int mode)
{
	const utf8 *sco;
  switch ( mode) {
    default:             sco = U8"initial"; break;;
  }
  ul_loginf( "Change op.mode to 0x%X -> scenario = %s\n", mode, (char*)sco);
  return sco;
}

/******************************************************************************/
/* Global timers & features */
/* propagate system id into sxml (id_system_id ==0 for RTEMS, ==1 for Linux, ==2 for W32) */
#ifdef WITH_RTEMS_UID	/* RTEMS */
static short system_id = 0;
#elif defined(WIN32)	/* Windows32 */
static short system_id = 2;
#else			/* Linux */
static short system_id = 1;
#endif
sui_dinfo_static( id_system_id_di, SUI_TYPE_SHORT, &system_id, sui_short_rdval, sui_short_wrval, 0, 0, 3, 0);

/* Current time */
unsigned long currenttime = 0;
sui_dinfo_static( time_dinfo, SUI_TYPE_ULONG, &currenttime, sui_ulong_rdval, sui_ulong_wrval, 0, 0, 0, 0);

/* get current time */
#ifdef WITH_RTEMS_UID	/* RTEMS */

#include <rtems.h>

unsigned long get_current_time (void)
{
  rtems_interval ticks;
 #ifndef RTEMS_CLOCK_GET_TICKS_SINCE_BOOT
  ticks = rtems_clock_get_ticks_since_boot();
 #else /*RTEMS_CLOCK_GET_TICKS_SINCE_BOOT*/
  rtems_clock_get(RTEMS_CLOCK_GET_TICKS_SINCE_BOOT,&ticks);
 #endif /*RTEMS_CLOCK_GET_TICKS_SINCE_BOOT*/
  return ticks;
}

#elif defined(WIN32)	/* WIN32 */

unsigned long get_current_time ( void)
{
  return GetTickCount();
}

#else	/* LINUX */

#include <sys/time.h>

unsigned long get_current_time( void)
{
  struct timeval time;
  struct timezone tz;
  unsigned long mstime;
  gettimeofday( &time, &tz);
  mstime = (time.tv_sec*1000) + (time.tv_usec/1000);
  return mstime;
}

#endif /* WIN32 */

static inline
void refresh_activity_time( sui_application_t *appl)
{
  appl->activity_time = get_current_time();
}

static inline
unsigned long get_time_from_last_activity( sui_application_t *appl)
{
  return ( get_current_time() - appl->activity_time);
}

int time_from_last_activity_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  unsigned long val;
  sui_application_t *appl;

  if(!dinfo->ptr)
    return SUI_RET_ERR;

  appl = (sui_application_t *)dinfo->ptr;

  val = get_time_from_last_activity(appl);

  *(unsigned long *)buf = val;
  return SUI_RET_OK;
}

ul_htim_time_t main_evpoll_get_current_time(ul_evpbase_t *base)
{
  struct timespec current_time;

  clock_gettime(CLOCK_MONOTONIC, &current_time);

  return current_time.tv_sec * 1000 + current_time.tv_nsec / 1000000;
}

/******************************************************************************/
/* Wall clock time */

unsigned long time_actual_epochsec;
signed long   time_local_offset;

int time_actual_epochsec_systime_get( unsigned long *pepochsec)
{
  struct timeval time;
  struct timezone tz;
  gettimeofday( &time, &tz);
  time_t time_sec;
  struct tm time_broken;

  if (!pepochsec) return -1;

  *pepochsec = time.tv_sec;
  time_sec = time.tv_sec;

  localtime_r(&time_sec, &time_broken);
 #ifndef WITH_RTEMS_UID	/* RTEMS */
  time_local_offset = time_broken.tm_gmtoff;
 #endif /*WITH_RTEMS_UID*/

  return 0;
}

int time_actual_epochsec_wrval(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  unsigned long newtime;
  struct timeval time;

  newtime = *( unsigned long *)buf;

  /* Consider zero as unacceptable time */
  if (!newtime)
    return SUI_RET_ERR;

  time.tv_sec = newtime;
  time.tv_usec = 0;

 #ifndef WITH_RTEMS_UID	/* RTEMS */
  /* Set system and RTC date and time */
  if (settimeofday(&time , NULL)<0)
    return SUI_RET_ERR;
 #endif /*WITH_RTEMS_UID*/

  /* save new value */
  if ( sui_ulong_wrval( dinfo, 0, &newtime))
    return SUI_RET_ERR;

  return SUI_RET_OK;
}

sui_dinfo_static( time_actual_epochsec_di, SUI_TYPE_ULONG, &time_actual_epochsec,
           sui_ulong_rdval, time_actual_epochsec_wrval,
	   0, 0, 0xffffffff, 0);

int time_local_epochsec_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  int ret;
  unsigned long val;
  unsigned long val_local;
  ret = sui_rd_ulong(&time_actual_epochsec_di, 0, &val);
  if (ret != SUI_RET_OK)
    return ret;
  val_local = val + time_local_offset;
  if ((time_local_offset < 0) && (val_local > val))
    val_local = 0;
  *(unsigned long *)buf = val_local;
  return SUI_RET_OK;
}

sui_dinfo_static( time_local_epochsec_di, SUI_TYPE_ULONG, NULL,
           time_local_epochsec_rdval, NULL,
	   0, 0, 0xffffffff, 0);

/******************************************************************************/
/* Time of the holded key */
unsigned short lastkey = 0;
unsigned long lastkeytime = 0;

int key_time_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  unsigned long dif;
  if ( lastkeytime) {
    dif = get_current_time() - lastkeytime;
    if ( dif > dinfo->maxval)
      dif=dinfo->maxval;
    *(unsigned long *)buf = dif;
  } else {
    *(unsigned long *)buf = 0;
  }
  return SUI_RET_OK;
}

sui_dinfo_static( key_time_di, SUI_TYPE_ULONG, &lastkeytime, key_time_rdval, NULL, 0, 0, 10000, 0);


/******************************************************************************/
/* application specific settings and connection GUI with logic */
int appl_register_globals(void)
{
  ns_add_object_to_ns( &ns_global_namespace, ns_create_object( "id_system_id", &id_system_id_di, SUI_TYPE_DINFO, 0));
  ns_add_object_to_ns( ns_current_namespace, ns_create_object( "time_actual_epochsec", &time_actual_epochsec_di, SUI_TYPE_DINFO, 0));
  ns_add_object_to_ns( ns_current_namespace, ns_create_object( "time_local_epochsec", &time_local_epochsec_di, SUI_TYPE_DINFO, 0));

  return 0;
}

/******************************************************************************/
long last_op_mode = 0;
sui_dinfo_static(appl_snap_op_mode_di, SUI_TYPE_LONG, &last_op_mode,
                  sui_long_rdval, NULL, 0, 0, 0, 0);


int main_state_check(sui_environment_t *env)
{
  /* update variable with current time */
  env->curapp->cur_time = get_current_time();
  /* update global blinking flag */
  if ( env->curapp->cur_time > env->curapp->blink_time) {
    env->sui_global_dc.dc_flags ^= SUDCF_BLINK_NOW;
    env->curapp->blink_time = env->curapp->cur_time + SUI_GLOBAL_BLINK_TIME;

  }

  /* change state checking */
  if ( last_op_mode != appl_op_mode) {
    last_op_mode = appl_op_mode;

    /*FIXME:/CHECKME: flush all pending events, they can cause problems with dangling pointers*/
    sui_flush_event();
    sui_flush_global_event();

    if ( sui_appl_change_scenario( env->curapp, get_scenario_from_mode( last_op_mode), &sxml_suitk_ddesc)<0) {
      /* FIXME: what will we do if error occurs ? */
      ul_logerr("sui_appl_change_scenario failed\n");
      return -1;
    }
    refresh_activity_time( env->curapp); /* FIXME: really clear usertime (no key pressed) ? - 2min.error */
  }

  /* screen state function */
  if (env->curapp->proc_fcn &&
       !(env->curapp->cstflg & SUI_APPL_FLAGS_FCN_NOPROC)) {
    if ( env->curapp->proc_fcn( env->curapp, env->curapp->cstate,
                                SUI_STATE_FCN_PROC)<0)
      env->curapp->cstflg |= SUI_APPL_FLAGS_FCN_NOPROC;
  }

  return 0;
}

/******************************************************************************/

int main_refresh_tick(sui_environment_t *env)
{

  sui_redraw_request( env->curapp->wdgroot);

  if ( !env->curapp->mdialog) { /* normal mode - no modal dialog mode */
    sui_transition_t *trtarget = sui_appl_check_transitions( env->curapp, /*event*/ NULL); /* FIXME: state/substate */
    if ( trtarget) {
      sui_event_t glev;
      ul_logdeb("TOCHANGESCREEN-NOevent\n");
      glev.what = SUEV_GLOBAL;
      glev.message.command = SUGM_CHANGE_SCREEN;
      glev.message.ptr = trtarget;
      sui_put_global_event( &glev);
    }
    /* check dialog conditions */
    {
      sui_ssdialog_t *dlg = sui_appl_check_ssdialog( env->curapp, /*event*/ NULL, SUI_SSD_COND_ENTRY);
      if ( dlg) {
              int ret = sui_appl_open_dialog( env->curapp, dlg);
              ul_logdeb("Open dialog (struct=%p,wdg=%p) ret=%d\n", dlg, dlg->dialog, ret);
      }
    }
  } else { /* modal dialog is opened */
    sui_ssdialog_t *dlg = sui_appl_check_ssdialog( env->curapp, /*event*/ NULL, SUI_SSD_COND_EXIT);
    if ( dlg) {
            int ret = sui_appl_close_dialog( env->curapp, 0);
            ul_logdeb("Close dialog by event (ret=%d)\n", ret);
    }
  }

  return 0;
}

/******************************************************************************/
int main_process_global_event(sui_environment_t *env, sui_event_t *event)
{
  int ret = 0;
  switch (event->message.command) {
    case SUGM_CHANGE_SCREEN:
      {
        sui_transition_t *target = event->message.ptr;
        sui_flush_event();
        sui_flush_global_event();
        if (target && (target->scenario || target->tostate ||
            target->tosubst || target->towidget)) {
          if (sui_appl_change_state(env->curapp, target, sui_change_local_namespace_from_sxml, &sxml_suitk_ddesc)) {
            ul_logerr("StateChanging ERROR\n");
            ret = -1;
          }
          sui_redraw_request(NULL);
        } else {
          /* no target of transition - process only actions (before and then after */
          if (target->abefore_head) {
            ul_logdeb("- Process BEFORE actions.\n");
            sui_action_process_all(env->curapp, target->abefore_head);
          }
          if (target->aafter_head) {
            ul_logdeb("- Process AFTER actions.\n");
            sui_action_process_all(env->curapp, target->aafter_head);
          }
        }
      }
      break;
    case SUGM_NOTIFY:
      if (env->curapp->mdialog) { /* modal dialog */
        int ret = sui_appl_close_dialog(env->curapp, event->message.info);
        ul_logdeb("Close dialog by button (ret=%d)\n", ret);
      }
      break;
  }
  refresh_activity_time(env->curapp);
  return ret;
}
/******************************************************************************/

int main_process_event(sui_environment_t *env, sui_event_t *event)
{

/* update last key time */
  if (event->what==SUEV_KDOWN && event->keydown.repeats==0) {
    lastkeytime = get_current_time();
    lastkey = event->keydown.keycode;
  }
  if (event->what==SUEV_KUP && event->keydown.keycode==lastkey) {
    lastkeytime = 0;
    lastkey = 0;
  }



  /* debug print - events */
#ifdef CONFIG_OC_WIDGET_DEVELOPMENT
  debug_print_current_event( event);
#endif /* CONFIG_OC_WIDGET_DEVELOPMENT */
  /* check and process event actions */
  if ( env->curapp->proc_fcn &&
      !(env->curapp->cstflg & SUI_APPL_FLAGS_FCN_NOEVENT)) {
    if ( env->curapp->proc_fcn( env->curapp, event, SUI_STATE_FCN_EVENT)<0)
      env->curapp->cstflg |= SUI_APPL_FLAGS_FCN_NOEVENT;
  }

  if ( event->what == SUEV_COMMAND) {
ul_loginf("CMD event: cmd=%d, ptr=%p, info=%ld\n", event->message.command, event->message.ptr, event->message.info);
    /* QUIT message !!! */
    switch(event->message.command) {
    case SUCM_QUIT:
      ul_logmsg("Program break...");
      break;
    case SUCM_NEXT:
      if ( !env->curapp->mdialog) {
        if ( !sui_appl_change_focus( env->curapp, 1))
          sui_clear_event( env->curapp->wdgroot, event);
      }
      break;
    case SUCM_PREV:
      if ( !env->curapp->mdialog) {
        if ( !sui_appl_change_focus( env->curapp, -1))
          sui_clear_event( env->curapp->wdgroot, event);
      }
      break;
    }
  }

  /* check hooked events */
  if( event->what == SUEV_GLOBAL) {
    if ( !main_process_global_event( env, event)) {
      sui_clear_event( env->curapp->wdgroot, event);
    } else {
      ul_logerr("main_process_global_event failed\n");
      return -1;
    }
  }

  if( event->what == SUEV_KDOWN) {
    refresh_activity_time( env->curapp);
#ifndef WITH_RTEMS_UID
    if( event->keydown.keycode == MWKEY_F8) {
      ul_logmsg( "User break...\n");
      return -2;
    }
//    if ( event->keydown.keycode == MWKEY_F11) {
//      ul_logdeb( "Print widget tree...\n");
//      sui_print_widget(env->curapp->wdgroot, 0);
//    }
    if ( event->keydown.keycode == MWKEY_F12) {
      ul_logdeb( "Clear widget tree focus ...\n");
      sui_appl_clear_focus( env->curapp);
    }
#endif /* WITH_RTEMS_UID */
  }
  if ( !env->curapp->mdialog) { /* normal screen - no modal dialog */
    sui_transition_t *trtarget = sui_appl_check_transitions( env->curapp, event);
    if ( trtarget) {
      sui_event_t glev;
      ul_logdeb("TOCHANGESCREEN-EVENT\n");
      glev.what = SUEV_GLOBAL;
      glev.message.command = SUGM_CHANGE_SCREEN;
      glev.message.ptr = trtarget;
      sui_put_global_event( &glev);
    }
    /* check dialog conditions */
    {
      sui_ssdialog_t *dlg = sui_appl_check_ssdialog( env->curapp, event, SUI_SSD_COND_ENTRY);
      if ( dlg) {
        int ret = sui_appl_open_dialog( env->curapp, dlg);
        ul_logdeb("Open dialog (struct=%p,wdg=%p) ret=%d\n", dlg, dlg->dialog, ret);
        sui_clear_event( NULL, event);
      }
    }
  } else { /* modal dialog is opened */
    sui_ssdialog_t *dlg = sui_appl_check_ssdialog( env->curapp, event, SUI_SSD_COND_EXIT);
    if ( dlg) {
      int ret = sui_appl_close_dialog( env->curapp, 0);
      ul_logdeb("Close dialog by event (ret=%d)\n", ret);
      sui_clear_event( NULL, event);
    }
  }

  /* for development */
#if defined(CONFIG_OC_WIDGET_DEVELOPMENT) // && MWPIXEL_FORMAT!=MWPF_PALETTE
  main_loop_debug_part( env->curapp, event);
#endif /*MWPIXEL_FORMAT!=MWPF_PALETTE*/

  /* process draw events */
  if ( event->what & (SUEV_DRAW|SUEV_REDRAW)) {
    sgm_lcd_out_lock(sui_globdc->psd);
    /* BE CAREFUL - change main screen offset */
    sui_globdc->offset.x = env->curapp->wdgroot->place.x;
    sui_globdc->offset.y = env->curapp->wdgroot->place.y;
    sui_hevent( env->curapp->wdgroot, event);
    sgm_lcd_out_unlock(sui_globdc->psd);
  } else {    /* all others events */
    ul_logdeb("other event - (%d)\n",event->what);
    sui_hevent( env->curapp->wdgroot, event);
  }

  return 0;
}

/******************************************************************************/
int main_check_transitions(sui_environment_t *env, sui_event_t *event)
{
  int ret=0;
  if ( env->curapp->cscenario) {
      sui_transition_t *trans = env->curapp->cscenario->trans_head;
    while( trans) {
      if ( !sui_appl_check_condition( env->curapp, trans->cond, event)) {
        sui_event_t glev;
        ul_logdeb("Scenario Transition fired\n");
        glev.what = SUEV_GLOBAL;
        glev.message.command = SUGM_CHANGE_SCREEN;
        glev.message.ptr = trans;
        sui_put_global_event( &glev);
	ret = 1;
	/*FIXME:/CHECKME: I would expect, that break should be there, because we cannot
	  have multiple transitions pending*/
	break;
      }
      trans = trans->next_trans;
    }
  }
  return ret;
}

/******************************************************************************/
int main_disp_loop_simple( sui_environment_t *env)
{
  sui_event_t event;
  int ret;

  refresh_activity_time( env->curapp);

  /* init last_op_mode */
  last_op_mode = appl_op_mode;

  do {

    main_state_check(env);

#ifdef WITH_RTEMS_UID
    /* Space for application idle/poll logic */
#endif /*WITH_RTEMS_UID*/

    /* from queue to process it */
    if( sui_get_event( &event, 1000) <= 0) {

      main_refresh_tick(env);

    } else {

      ret = main_process_event(env, &event);
      if (ret < 0)
        break;

    }

    main_check_transitions(env, &event);

  } while( 1);
  return 0;
}

/******************************************************************************/
typedef struct main_events_anchor_t {
  ul_evptrig_t evtrig;
  sui_environment_t *env;
  ul_htim_diff_t timeout;
} main_events_anchor_t;

void main_events_cb(ul_evptrig_t *evtrig, int what)
{
  main_events_anchor_t *anchor = UL_CONTAINEROF(evtrig, main_events_anchor_t, evtrig);
  sui_environment_t *env = anchor->env;
  sui_event_t event;
  int ret;
  int refresh = 0;

  if(what & UL_EVP_DONE) {
    ul_evptrig_done(evtrig);
    free(anchor);
    return;
  }

  if(what & UL_EVP_TIMEOUT)
    refresh = 1;

  if(what & UL_EVP_READ)
    sui_globev.sui_check_kbd = 1;

  time_actual_epochsec_systime_get(&time_actual_epochsec);

  do {
    /* from queue to process it */
    ret = sui_get_event( &event, 0);
    if (ret <= 0) {
      if(!refresh)
        break;
      refresh = 0;
      main_refresh_tick(env);
    } else {
      ret = main_process_event(env, &event);
      if (ret == -2) {
	ul_evpoll_quilt_loop(ul_evptrig_get_base(evtrig));
	break;
      }
    }
    main_check_transitions(env, &event);
    main_state_check(env);
  } while(1);

  if(anchor->timeout) {
    ul_htim_diff_t timeout;
    ul_htim_diff_t blink_timeout;
    timeout = anchor->timeout;
    blink_timeout = env->curapp->blink_time - env->curapp->cur_time + 1;
    if(timeout > blink_timeout)
       timeout = blink_timeout;
    if(curevbufs->draw_request || (timeout < 50))
      timeout = 50;

    ul_logtrash("=== setting timeout %ld ===\n", (long)timeout);
    ul_evptrig_set_timeout(evtrig, &timeout);
  }
}

int main_setup_disp_loop( sui_environment_t *env)
{
  main_events_anchor_t *kbd_anchor;
  main_events_anchor_t *mouse_anchor;

  refresh_activity_time( env->curapp);

  /* init last_op_mode */
  last_op_mode = appl_op_mode;

  kbd_anchor = malloc(sizeof(*kbd_anchor));
  if(!kbd_anchor) {
    ul_logerr("cannot allocate kbd_anchor event\n");
    goto error_kbd_alloc;
  }

  if(ul_evptrig_init(NULL, &kbd_anchor->evtrig)<0) {
    ul_logerr("cannot init kbd_anchor event\n");
    goto error_kbd_init;
  }

  kbd_anchor->env = env;
  kbd_anchor->timeout = 1000;

  if(ul_evptrig_set_and_arm_fd(&kbd_anchor->evtrig, main_events_cb, sui_globev.sui_kbd_fd,
                             UL_EVP_READ, &kbd_anchor->timeout, 0)<0) {
    ul_logerr("cannot arm kbd_anchor event\n");
    goto error_kbd_arm;
  }


  if(sui_globev.sui_mouse_fd>=0) {
    mouse_anchor = malloc(sizeof(*mouse_anchor));
    if(!mouse_anchor) {
      ul_logerr("cannot allocate mouse_anchor event\n");
      goto error_mouse_alloc;
    }

    if(ul_evptrig_init(NULL, &mouse_anchor->evtrig)<0) {
      free(mouse_anchor);
      ul_logerr("cannot init mouse_anchor event\n");
      goto error_mouse_init;
    }

    mouse_anchor->env = env;
    mouse_anchor->timeout = 0;

    if(ul_evptrig_set_and_arm_fd(&mouse_anchor->evtrig, main_events_cb, sui_globev.sui_mouse_fd,
                               UL_EVP_READ, NULL, 0)<0) {
      ul_logerr("cannot arm mouse_anchor event\n");
      goto error_mouse_arm;
    }
  }

  main_state_check(env);

  return 0;


  error_mouse_arm:
    ul_evptrig_done(&mouse_anchor->evtrig);
  error_mouse_init:
    free(mouse_anchor);
  error_mouse_alloc:
  error_kbd_arm:
    ul_evptrig_done(&kbd_anchor->evtrig);
  error_kbd_init:
    free(kbd_anchor);
  error_kbd_alloc:
    return -1;
}

/******************************************************************************/
/**
 * main
 */
#ifndef WITH_RTEMS_UID
int main( int argc, char *argv[])
#else /*WITH_RTEMS_UID*/
int ui_main( int argc, char *argv[])
#endif /*WITH_RTEMS_UID*/
{
  sui_environment_t *desktop = NULL;
  utf8 *appl_path = NULL;

  char *filename = NULL;

  if ( argc>1)
    filename = strdup( argv[1]); /* we need it dynamical */
  else {
    ul_logerr( "SXML file not specified, it has to be provided to application\n");
    return 1;
  }

/* FIXME: for RTEMS target is in "init.c" */
/*#ifndef WITH_RTEMS_UID*/
  // register default list of types
  sui_type_register_default_types();
/*#endif*/

  /* preset global namespace */
  ns_set_global_namespace();

#ifndef WITH_RTEMS_UID
/*FIXME*/
/*
  if( id_nsregister() < 0) {
    printf("id_nsregister failed \n");
    return 1;
  }

  kvpb_simulation_stub_init();
*/
#endif

 #ifndef WITH_RTEMS_UID
  {
    ul_evpbase_t *evpbase;
    static ul_evpoll_ops_t new_ops;
    evpbase = ul_evpoll_chose_implicit_base();
    if (!evpbase) {
      ul_logerr("Cannot setup event poll base!\n");
      return 1;
    }
    new_ops = *evpbase->ops;
    new_ops.base_get_current_time = main_evpoll_get_current_time;
    ul_evpoll_ops_implicit = &new_ops;
    evpbase->ops = ul_evpoll_ops_implicit;
  }
 #else /*WITH_RTEMS_UID*/
  appl_use_simple_main_loop = 1;
 #endif /*WITH_RTEMS_UID*/

//  ns_add_object_to_ns( ns_current_namespace, ns_create_object( "snap_op_mode", &appl_snap_op_mode_di, SUI_TYPE_DINFO, 0));

//  tsg_init();

  if(appl_register_globals()<0) {
    ul_logerr("Global object registration failed!\n");
    return 1;
  }

//  if(appl_register_statedinfos()<0) {
//    ul_logerr("State DINFOs registration failed!\n");
//    return 1;
//  }

/*
#ifndef WITH_RTEMS_UID
  ns_add_object_to_ns( ns_current_namespace, ns_create_object( "idui_hkey_trans_alarm_key", idui_hkey_trans_alarm_key, SUI_TYPE_KEYFCN, NSOF_STATIC_ALL));

#endif
*/
  /* register all suitk defines */
  suiuser_register_all_suitk_defines();
  /* register static data and function */
  //	suiuser_register_all_action_functions();
  register_all_state_variables();

//  xmlrpcservices_init();

  // get application path - path to init sxml file
  {
    int zero_fn = sui_get_part_of_path( filename, -1, '/', 1);
    if ( zero_fn < 0)
    {
      ul_logerr("Application path wasn't parsed!\n");
      goto sxml_parse_eof;
    }
    if ( zero_fn > 0)
    {
      appl_path = sui_utf8( NULL, -1, zero_fn+1);
      sui_utf8_ncpy( appl_path, U8(filename), zero_fn);
    }
    else
    {
      appl_path = sui_utf8(U8CH"./", -1, -1);
    }
    //ul_logdeb("Appl.path='%s'\n",sui_utf8_get_text( appl_path));
  }

  /* sxml file parsing */
  /* if ( sui_sxml_parse_document(&sxml_suitk_ddesc, filename, SUI_FILE_SOURCE_FILE)) */
  if ( sui_sxml_parse_document(&sxml_suitk_ddesc, filename, NULL))
  {
    ul_logerr("Parsing SXML document error!\n");
    goto sxml_parse_eof;
  }

  /* open screen test */
  {
    sui_environment_open_params_t envparams = {SUI_ENVOPEN_FL_MOUSE_OPTIONAL, sui_fonts};
    if ( !(desktop = sui_environment_open(&envparams))) {
      ul_logerr("Desktop opening\n");
      return 1;
    }
  }
  if ( sui_environment_create_application( desktop, "application", appl_path) < 0) {
    ul_logerr("Application wasn't initialized!\n");
    return 2;
  }
  sui_utf8_dec_refcnt( appl_path);

/* init mobil keyboard */
  sui_mk_init();

  {
    sui_dinfo_t *di;
    di = sui_create_dinfo(desktop->curapp, 0, 0, 0, 0, time_from_last_activity_rdval, NULL);
    if(di != NULL) {
      di->idxsize = 1;
      di->tinfo = SUI_TYPE_ULONG;
      ns_add_object_to_ns( ns_current_namespace,
			ns_create_object( "time_from_last_activity", di, SUI_TYPE_DINFO, 0));
      sui_dinfo_dec_refcnt(di);
    }
  }

  if ( sui_appl_change_scenario( desktop->curapp, U8"initial", &sxml_suitk_ddesc)<0) { /* "stop,initial" */
    return -1;
  }

// program_loop
  if(appl_use_simple_main_loop) {
    main_disp_loop_simple(desktop);
  }
 #ifndef WITH_RTEMS_UID
  else {
    int ret;
    ret = main_setup_disp_loop(desktop);
    if(ret >= 0) {
      ret=ul_evpoll_loop(NULL, 0);
      ul_logmsg("ul_evpoll_loop returned %d\n", ret);
    } else {
      ul_logerr("main_setup_disp_loop failed\n");
    }
  }
 #endif /*WITH_RTEMS_UID*/
sxml_parse_eof:

#ifdef WITH_RTEMS_UID
  /*fatal_error_screen_and_loop(desktop);*/
#endif /*WITH_RTEMS_UID*/

/* destroy mobil keyboard */
  sui_mk_done();

  // close environment
  if ( sui_environment_close( desktop))
    ul_logerr("Desktop closing\n");

  //ul_logdeb("\nGlobal namespace\n");
  //	ns_print_namespace( &ns_global_namespace);
  //ul_logdeb("\nLocal namespace\n");
  //	ns_print_namespace( &ns_local_namespace);

  ul_logdeb ("Destroy namespaces\n");
  ns_clear_namespace( &ns_local_namespace);	// ns_destroy_local_namespace();
  ns_clear_namespace( &ns_global_namespace);	// ns_destroy_global_namespace();
  if (filename) free( filename);
#if defined(CONFIG_OC_WIDGET_DEVELOPMENT)
  debug_print_object_counts(0);
  //  EndMemoryStatus(0);
  //  ul_logdeb( "\nPress ENTER.\n");
  //  getchar();
#endif /*CONFIG_OC_WIDGET_DEVELOPMENT*/
  return 0;
}
