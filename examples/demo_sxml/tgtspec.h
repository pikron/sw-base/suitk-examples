#ifndef TARGET_SPECIFIC_ISSUES_HEADER_FILE
#define TARGET_SPECIFIC_ISSUES_HEADER_FILE

#include <suitk/suitk.h>
#include <suixml/sui_defines.h>

int sgm_lcd_out_lock(PSD psd);
int sgm_lcd_out_unlock(PSD psd);

int register_all_state_variables(void);

#endif /* TARGET_SPECIFIC_ISSUES_HEADER_FILE */
