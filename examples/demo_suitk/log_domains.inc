/*
 * This is generated file, do not edit it directly.
 * Take it from standard output of "ul_log_domains"
 * script called in the top level project directory
 */

ul_log_domain_t ulogd_appfncs	= {UL_LOGL_DEF, "appfncs"};
ul_log_domain_t ulogd_applutils	= {UL_LOGL_DEF, "applutils"};
ul_log_domain_t ulogd_barcode	= {UL_LOGL_DEF, "barcode"};
ul_log_domain_t ulogd_evpoll	= {UL_LOGL_DEF, "evpoll"};
ul_log_domain_t ulogd_mainappl	= {UL_LOGL_DEF, "mainappl"};
ul_log_domain_t ulogd_mymalloc	= {UL_LOGL_DEF, "mymalloc"};
ul_log_domain_t ulogd_remote_cmd	= {UL_LOGL_DEF, "remote_cmd"};
ul_log_domain_t ulogd_rs232fn	= {UL_LOGL_DEF, "rs232fn"};
ul_log_domain_t ulogd_statedinfos	= {UL_LOGL_DEF, "statedinfos"};
ul_log_domain_t ulogd_suitk	= {UL_LOGL_DEF, "suitk"};
ul_log_domain_t ulogd_suiut	= {UL_LOGL_DEF, "suiut"};
ul_log_domain_t ulogd_suixml	= {UL_LOGL_DEF, "suixml"};
ul_log_domain_t ulogd_sxml	= {UL_LOGL_DEF, "sxml"};
ul_log_domain_t ulogd_xmlrpcglue	= {UL_LOGL_DEF, "xmlrpcglue"};
ul_log_domain_t ulogd_xmlrpcservices	= {UL_LOGL_DEF, "xmlrpcservices"};

ul_log_domain_t *ul_log_domains_array[] = {
  &ulogd_appfncs,
  &ulogd_applutils,
  &ulogd_barcode,
  &ulogd_evpoll,
  &ulogd_mainappl,
  &ulogd_mymalloc,
  &ulogd_remote_cmd,
  &ulogd_rs232fn,
  &ulogd_statedinfos,
  &ulogd_suitk,
  &ulogd_suiut,
  &ulogd_suixml,
  &ulogd_sxml,
  &ulogd_xmlrpcglue,
  &ulogd_xmlrpcservices,
};
