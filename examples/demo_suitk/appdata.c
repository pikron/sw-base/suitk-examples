/* application data */

#include "appdata.h"

#include <ul_log.h>
UL_LOG_CUST(ulogd_mainappl)

/******************************************************************************/
/* System ID */
/* propagate system id into sxml (id_system_id ==0 for RTEMS, ==1 for Linux, ==2 for W32) */
#ifdef WITH_RTEMS_UID	/* RTEMS */
static short system_id = 0;
#elif defined(WIN32)	/* Windows32 */
static short system_id = 2;
#else			/* Linux */
static short system_id = 1;
#endif
sui_dinfo_static( id_system_id_di, SUI_TYPE_SHORT, &system_id, sui_short_rdval, sui_short_wrval, 0, 0, 3, 0);

/******************************************************************************/
/* Current time */
unsigned long currenttime = 0;
sui_dinfo_static( time_dinfo, SUI_TYPE_ULONG, &currenttime, sui_ulong_rdval, sui_ulong_wrval, 0, 0, 0, 0);

/* get current time */
#ifdef WITH_RTEMS_UID	/* RTEMS */

#include <rtems.h>

unsigned long get_current_time (void)
{
  rtems_interval ticks;
 #ifndef RTEMS_CLOCK_GET_TICKS_SINCE_BOOT
  ticks = rtems_clock_get_ticks_since_boot();
 #else /*RTEMS_CLOCK_GET_TICKS_SINCE_BOOT*/
  rtems_clock_get(RTEMS_CLOCK_GET_TICKS_SINCE_BOOT,&ticks);
 #endif /*RTEMS_CLOCK_GET_TICKS_SINCE_BOOT*/
  return ticks;
}

#elif defined(WIN32)	/* WIN32 */

unsigned long get_current_time ( void)
{
  return GetTickCount();
}

#else	/* LINUX */

#include <sys/time.h>

unsigned long get_current_time( void)
{
  struct timeval time;
  struct timezone tz;
  unsigned long mstime;
  gettimeofday( &time, &tz);
  mstime = (time.tv_sec*1000) + (time.tv_usec/1000);
  return mstime;
}

#endif /* WIN32 */

void refresh_activity_time( sui_application_t *appl)
{
  appl->activity_time = get_current_time();
}

static inline
unsigned long get_time_from_last_activity( sui_application_t *appl)
{
  return ( get_current_time() - appl->activity_time);
}

int time_from_last_activity_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  unsigned long val;
  sui_application_t *appl;

  if(!dinfo->ptr)
    return SUI_RET_ERR;

  appl = *(sui_application_t **)dinfo->ptr;

  val = get_time_from_last_activity(appl);

  *(unsigned long *)buf = val;
  return SUI_RET_OK;
}

extern sui_application_t *current_appl;

sui_dinfo_static(time_from_last_activity_di, SUI_TYPE_ULONG, &current_appl,
                 time_from_last_activity_rdval, NULL, 0, 0, 0, 0);

ul_htim_time_t main_evpoll_get_current_time(ul_evpbase_t *base)
{
  struct timespec current_time;

  clock_gettime(CLOCK_MONOTONIC, &current_time);

  return current_time.tv_sec * 1000 + current_time.tv_nsec / 1000000;
}

/******************************************************************************/
/* Wall clock time */

unsigned long time_actual_epochsec;
signed long   time_local_offset;

int time_actual_epochsec_systime_get(void)
{
  struct timeval time;
  struct timezone tz;
  gettimeofday( &time, &tz);
  time_t time_sec;
  struct tm time_broken;

  time_actual_epochsec = time.tv_sec;
  time_sec = time.tv_sec;

  localtime_r(&time_sec, &time_broken);
 #ifndef WITH_RTEMS_UID	/* RTEMS */
  time_local_offset = time_broken.tm_gmtoff;
 #endif /*WITH_RTEMS_UID*/

  return 0;
}

int time_actual_epochsec_wrval(sui_dinfo_t *dinfo, long indx, const void *buf)
{
  unsigned long newtime;
  struct timeval time;

  newtime = *( unsigned long *)buf;

  /* Consider zero as unacceptable time */
  if (!newtime)
    return SUI_RET_ERR;

  time.tv_sec = newtime;
  time.tv_usec = 0;

 #ifndef WITH_RTEMS_UID	/* RTEMS */
  /* Set system and RTC date and time */
  if (settimeofday(&time , NULL)<0)
    return SUI_RET_ERR;
 #endif /*WITH_RTEMS_UID*/

  /* save new value */
  if ( sui_ulong_wrval( dinfo, 0, &newtime))
    return SUI_RET_ERR;

  return SUI_RET_OK;
}

sui_dinfo_static( time_actual_epochsec_di, SUI_TYPE_ULONG, &time_actual_epochsec,
           sui_ulong_rdval, time_actual_epochsec_wrval,
	   0, 0, 0xffffffff, 0);

int time_local_epochsec_rdval(sui_dinfo_t *dinfo, long indx, void *buf)
{
  int ret;
  unsigned long val;
  unsigned long val_local;
  ret = sui_rd_ulong(&time_actual_epochsec_di, 0, &val);
  if (ret != SUI_RET_OK)
    return ret;
  val_local = val + time_local_offset;
  if ((time_local_offset < 0) && (val_local > val))
    val_local = 0;
  *(unsigned long *)buf = val_local;
  return SUI_RET_OK;
}

sui_dinfo_static( time_local_epochsec_di, SUI_TYPE_ULONG, NULL,
           time_local_epochsec_rdval, NULL,
	   0, 0, 0xffffffff, 0);

/******************************************************************************/
/* FIXME: hack to run suitk without sxml - it should be corrected in the SuiTk */
int sui_change_local_namespace_from_sxml(utf8 *name, void *par)
{
    return -1;
}
int sui_term_resolve_id(char *id, sui_term_value_t *value)
{
    return -1;
}

SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_define_vmt_data;
SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_link_vmt_data;


/******************************************************************************/
sui_widget_t *root_widget = NULL;
sui_style_t *sty_basic = NULL, *sty_3d = NULL, *sty_noframe = NULL, *sty_labout;
sui_style_t *sty_small = NULL;
sui_colortable_t *coltab = NULL;

sui_list_t *demolist;


int btnnextval = 0;
sui_dinfo_t di_ctrl_next;

int btnnext_wrval(sui_dinfo_t *dinfo, long indx, const void *buf)
{
    btnnextval = *(long *)buf;
    sui_hkey_trans_global(root_widget, (SUGM_CHANGE_SCREEN<<16 | btnnextval));
    return SUI_RET_OK;
}
sui_dinfo_static(di_ctrl_next, SUI_TYPE_INT, &btnnextval, sui_int_rdval, btnnext_wrval, 0, 0, 0, 0);

int listval = 0;
sui_dinfo_static(di_ctrl_listpos, SUI_TYPE_INT, &listval, sui_int_rdval, sui_int_wrval, 0, 0, 0, 0);

/* main widget */

#define SCREEN_SX 320
#define SCREEN_SY 240

/* colors */
#define black 0x000000
#define white 0xffffff
#define redd  0x800000
#define redl  0xff0000
#define blued 0x000080
#define bluel 0x0000ff
#define greend 0x008000
#define greenl 0x00ff00
#define yellowd 0x808000
#define yellowl 0xffff00
#define silver  0xa0a0a0
#define greyl   0xc0c0c0
#define greyd   0x606060
#define txtnobg 0xff000000
#define bluedisp 0x1770b9

/* colortable */
/* COLUMNS: BackGround, Frame, Frame2, Item, Item2, LabelBack, Label */
/* ROWS: normal, highlighted, selected, higlighted& selected */
/* after 4*7 color - three special: tooltip_back, tooltip, shadow */
sui_color_t ctab[SUI_SYS_COLORS] = {
    silver, greyl, greyd, redl, white, silver, black,
    silver, white, greyl, silver, greyl, silver, greyd,
    0x0000ff00, 0x00ffffff, 0x008080, 0x00ffff, 0x4080c0, 0x80c0ff, white,
    0x00ffff00, greenl, greyl, bluedisp, white, txtnobg, greyl,
    black, yellowl, black
};

sui_key_action_t shortkey[] = {
    { MWKEY_DOWN, 0, 0, sui_hkey_trans_cmd, SUCM_NEXT},
    { MWKEY_UP, 0, 0, sui_hkey_trans_cmd, SUCM_PREV},
    { MWKEY_ENTER, 0, 0, sui_hkey_trans_cmd, SUCM_OK},
    { MWKEY_ESCAPE, 0, 0, sui_hkey_trans_cmd, SUCM_CANCEL},
    { 0, 0, 0, NULL, 0}
};

sui_finfo_t demo_time_fmt = {
    .refcnt = SUI_STATIC,
    .flags = SUFI_TIME,
    .base = 0, .digits = 0, .litstep = 0, .bigstep = 0,
    .ftext = U8"%d.%m.%Y %H:%M:%S",
    .choices = NULL
};

/* -------------------------------------------------------------------------- */
/* demo pages */
enum demo_pages {
    DEMO_PAGE_SELECT  = 1,
    DEMO_PAGE_BUTTONS = 2,
    DEMO_PAGE_NUMBERS = 3,
    DEMO_PAGE_ALIGN   = 4,
};

// ikona 4
MWIMAGEBITS img_ikona[] = {
//    0x00,0x00,0x00,0x00,
//    0x00,0x00,0x00,0x11,
//    0x00,0x00,0x00,0x11,
//    0x00,0x00,0x00,0x11,
//    0x00,0x00,0x01,0x10,
//    0x00,0x00,0x01,0x10,
//    0x00,0x00,0x01,0x10,
//    0x00,0x00,0x01,0x10,
//    0x00,0x00,0x11,0x00,
//    0x11,0x00,0x11,0x00,
//    0x11,0x01,0x10,0x00,
//    0x01,0x11,0x10,0x00,
//    0x00,0x11,0x00,0x00,
//    0x00,0x00,0x00,0x00,
//    0x23,0x23,0x23,0x23,
//    0x00,0x00,0x00,0x00,

    0x0000,0x0000,
    0x0000,0x0000,
    0x0000,0x0011,
    0x0000,0x0011,
    0x0000,0x0011,
    0x0000,0x0110,
    0x0000,0x0110,
    0x0000,0x0110,
    0x0000,0x0110,
    0x0000,0x1100,
    0x1100,0x1100,
    0x1101,0x1000,
    0x0111,0x1000,
    0x0011,0x0000,
    0x0000,0x0000,
    0x0000,0x0000,

    0x0000,0x0000,
    0x0000,0x0000,
    0x2200,0x0022,
    0x2200,0x0022,
    0x0220,0x0220,
    0x0220,0x0220,
    0x0022,0x2200,
    0x0022,0x2200,
    0x0002,0x2000,
    0x0022,0x2200,
    0x0220,0x0220,
    0x0220,0x0220,
    0x2200,0x0022,
    0x2200,0x0022,
    0x0000,0x0000,
    0x0000,0x0000,
};

sui_scenario_t demo_scena_select = {.refcnt=SUI_STATIC, .init = NULL, .trans_head = NULL}; //, .state_root=???};



// pripravi zakladni struktury (styly, fonty, ...) a hlavni okno
int demo_init(sui_environment_t *desktop, sui_application_t *appl)
{
    sui_rect_t box;
    sui_font_info_t *fi_v18b, *fi_tah, *fi_x57;
    sui_key_table_t *keys;

    // create fontinfo
    fi_v18b = sui_fonti_create(U8"Verdana18B", 0, 0);
    fi_tah = sui_fonti_create(U8"tahoma", 0, 0);
    fi_x57 = sui_fonti_create(U8"X5x7", 0, 0);
    // create colortable
    coltab = sui_colortable_create(/*SUI_STATIC*/ -SUI_SYS_COLORS, ctab);
    // create style
    sty_basic = sui_style_create(SUSFL_SEEMS3D, coltab, fi_v18b, fi_tah, 1, 1);
    sty_basic->labalign = SUAL_CENTER | SUAL_BLOCK;
    sty_3d = sui_style_create(SUSFL_FRAME | SUSFL_ROUNDEDFRAME | SUSFL_SHADOW | SUSFL_SEEMS3D | SUSFL_FOCUSLABEL | SUSFL_FOCUSFRAME,
                              coltab, fi_tah, fi_v18b, 2, 2);
    //sty_3d->labalign = SUAL_RIGHT | SUAL_CENTER | SUAL_INSIDE;
    sty_noframe = sui_style_create(SUSFL_FOCUSLABEL | SUSFL_TRANSPARENT, coltab, fi_v18b, fi_tah, 0, 2);
    sty_labout = sui_style_create(SUSFL_FRAME | SUSFL_FOCUSFRAME, coltab, fi_v18b, fi_tah, 1, 2);
    sty_labout->labalign = SUAL_TOP | SUAL_BLOCK;
    sty_small = sui_style_create(SUSFL_FRAME, coltab, fi_x57, fi_x57, 1, 1);
    // create key table
    keys = sui_key_table_create(shortkey);

// create widget
    box.x = 0; box.y = 0;
    box.w = SCREEN_SX; box.h = SCREEN_SY;
    root_widget = sui_wgroup(&box, NULL, sty_basic, SUFL_FOCUSEN);
    sui_widget_assign_key_table(root_widget, keys);
//    sui_inc_refcnt(root_widget);

    sui_fonti_dec_refcnt(fi_v18b);
    sui_fonti_dec_refcnt(fi_tah);
    sui_key_table_dec_refcnt(keys);

    desktop->curapp = appl;
    curevbufs = &(desktop->curapp->events);
    sui_inc_refcnt(root_widget);
    appl->wdgroot = root_widget;

    // prepare list
    demolist = (sui_list_t *) sui_obj_new_vmt( (sui_obj_vmt_t *)&sui_list_vmt_data);
    {
	sui_list_item_t *item = sui_list_item_create(U8"Test Buttons", NULL, NULL, NULL);
	sui_list_item_add(demolist, item, DEMO_PAGE_BUTTONS);
	item = sui_list_item_create(U8"Test Numbers", NULL, NULL, NULL);
	sui_list_item_add(demolist, item, DEMO_PAGE_NUMBERS);
	item = sui_list_item_create(U8"Test Alignment", NULL, NULL, NULL);
	sui_list_item_add(demolist, item, DEMO_PAGE_ALIGN);
	
	item = sui_list_item_create(NULL, &time_actual_epochsec_di/*&time_dinfo*/, &demo_time_fmt, NULL);
	sui_list_item_add(demolist, item, -1);
    }


    demo_openpage(desktop, DEMO_PAGE_SELECT);
    return 0;
}

int demo_done(void)
{
    if (root_widget) sui_dec_refcnt(root_widget);
    if (sty_basic) sui_style_dec_refcnt(sty_basic);
    if (sty_3d) sui_style_dec_refcnt(sty_3d);
    if (sty_labout) sui_style_dec_refcnt(sty_labout);
    if (sty_small) sui_style_dec_refcnt(sty_small);
    if (coltab) sui_colortable_dec_refcnt(coltab);
    if (demolist) sui_list_dec_refcnt(demolist);
    return 0;
}

int demo_openpage(sui_environment_t *desktop, int pageid)
{
    sui_rect_t box;
    sui_widget_t *wdg, *wdg1, *wdgnext;
    int nextpage = DEMO_PAGE_SELECT;

    box.x = 220; box.y = 210; box.w = 80; box.h = 22;
    wdgnext = sui_wbutton_with_dinfo(&box, U8"NEXT ->", sty_3d, SUFL_FOCUSEN | SUBF_PUSH_BTN | SUBF_USEVALUE, 10, yellowl, &di_ctrl_next);
    sui_group_add_widget_dec_refcnt(root_widget, wdgnext, SUGO_FOREGROUND);
    //sui_widget_connect(wdg, SUI_SIG_PRESSED, desktop->curapp)

    switch (pageid) {
    case DEMO_PAGE_SELECT:
        box.x = 10; box.y = 20; box.w = 300; box.h = 180;
        wdg = sui_wlistbox( &box, U8"Select a demo", sty_labout,
                    SUFL_FOCUSEN | SULF_CHANGE_FOCUS | SULF_MOBILKBD | SUFL_EDITEN,
                    demolist, &di_ctrl_listpos, NULL, 5, 1,
                    SUAL_RIGHT, 1);

        sui_group_add_widget_dec_refcnt(root_widget, wdg, SUGO_FOREGROUND);
    
        nextpage = DEMO_PAGE_BUTTONS;
        break;

    case DEMO_PAGE_BUTTONS:
        box.x = 10; box.y = 10; box.w = 120; box.h = 22;
        wdg1 = sui_wbutton(&box, U8"PUSH Btn", sty_3d, SUFL_FOCUSEN | SUBF_PUSH_BTN, 10, yellowl);
        sui_group_add_widget(root_widget, wdg1, SUGO_FOREGROUND);

        box.x = 10; box.y = 40; box.w = 120; box.h = 22;
        wdg = sui_wbutton(&box, U8"Light Btn", sty_3d, SUFL_FOCUSEN | SUBF_LIGHT_BTN, 14, yellowl);
        sui_group_add_widget_dec_refcnt(root_widget, wdg, SUGO_FOREGROUND);

        sui_widget_connect(wdg1, SUI_SIG_PRESSED, wdg, SUI_SLOT_TOGGLE);

        box.x = 10; box.y = 70; box.w = 180; box.h = 70;
        wdg1 = sui_wgroup(&box, U8 "Choice", sty_3d, SUFL_FOCUSEN);

        box.x = 20; box.y = 20; box.w = 120; box.h = 20;
        wdg = sui_wbutton(&box, U8"Option 1", sty_noframe, SUFL_FOCUSEN | SUBF_RADIO_BTN | SUBF_GROUPED, 10, yellowd);
        sui_group_add_widget_dec_refcnt(wdg1, wdg, SUGO_FOREGROUND);
        box.x = 20; box.y = 45; box.w = 120; box.h = 20;
        wdg = sui_wbutton(&box, U8"Option 2", sty_noframe, SUFL_FOCUSEN | SUBF_RADIO_BTN | SUBF_GROUPED, 10, yellowd);
        sui_group_add_widget_dec_refcnt(wdg1, wdg, SUGO_FOREGROUND);

        sui_group_add_widget_dec_refcnt(root_widget, wdg1, SUGO_FOREGROUND);

        sui_image_t *img = sui_image_create(8, 16, 2, SUIM_TRANSPARENT | SUIM_BITMAP_4BIT, img_ikona, coltab);
        box.x = 200; box.y = 10; box.w = 100, box.h = 40;
        wdg = sui_wbutton(&box, U8"ImgBtn", sty_3d, SUFL_FOCUSEN | SUBF_LIGHT_BTN | SUBF_USEIMAGE, 20, greenl);
        sui_btnwdg(wdg)->image = img;
        sui_group_add_widget_dec_refcnt(root_widget, wdg, SUGO_FOREGROUND);

        nextpage = DEMO_PAGE_NUMBERS;
        break;

    case DEMO_PAGE_NUMBERS:
        nextpage = DEMO_PAGE_ALIGN;
        break;

    case DEMO_PAGE_ALIGN:
        box.x = 10; box.y = 10; box.w = 120; box.h = 22;
        wdg = sui_wbutton(&box, U8"RIGHT", sty_3d, SUFL_FOCUSEN | SUBF_CHECK_BTN, 10, yellowl);
        sui_group_add_widget_dec_refcnt(root_widget, wdg, SUGO_FOREGROUND);

	nextpage = DEMO_PAGE_SELECT;
	break;

    default:
        ul_logerr("Require unknown demo page\n");
        return -1;
        break;
    }

    sui_btnwdg(wdgnext)->value = nextpage;

    sui_widget_init(root_widget, &desktop->sui_global_dc);
//    wdg = sui_group_find_focused_widget(root_widget, NULL);
    wdg = sui_group_find_next(root_widget, NULL, 0, 0);
ul_loginf("focused %p\n", wdg);
    sui_appl_set_focus(desktop->curapp, wdg);
    return 0;
}

int demo_closepage(void)
{
//    sui_widget_t *wdg;
    if (root_widget==NULL) return -1;
    while (sui_grpwdg(root_widget)->children) {
        // remove and destroy all root's children
        sui_group_remove_widget(sui_grpwdg(root_widget)->children);
    }
    return 0;
}
