#ifndef TARGET_SPECIFIC_ISSUES_HEADER_FILE
#define TARGET_SPECIFIC_ISSUES_HEADER_FILE

#include <suitk/suitk.h>

int sgm_lcd_out_lock(PSD psd);
int sgm_lcd_out_unlock(PSD psd);

#endif /* TARGET_SPECIFIC_ISSUES_HEADER_FILE */
