#ifndef APPLICATION_DATA
#define APPLICATION_DATA

#include <time.h>
#include <ul_evpoll.h>
#include <suitk/suitk.h>

/* System ID */
extern sui_dinfo_t id_system_id_di;

/* Time */
unsigned long get_current_time (void);
void refresh_activity_time( sui_application_t *appl);
ul_htim_time_t main_evpoll_get_current_time(ul_evpbase_t *base);

extern sui_dinfo_t time_dinfo;
extern sui_dinfo_t time_from_last_activity_di;

/* Wall clock time */
int time_actual_epochsec_systime_get(void);

extern sui_dinfo_t time_actual_epochsec_di;
extern sui_dinfo_t time_local_epochsec_di;

/* required dummy issues */
int sui_change_local_namespace_from_sxml(utf8 *name, void *par);
int sui_term_resolve_id(char *id, sui_term_value_t *value);

extern int listval;
extern sui_dinfo_t di_ctrl_listpos;

/* -------------------------------------------------------------------------- */
int demo_init(sui_environment_t *desktop, sui_application_t *appl);
int demo_done(void);
int demo_openpage(sui_environment_t *desktop, int pageid);
int demo_closepage(void);

#endif /* APPLICATION_DATA */
